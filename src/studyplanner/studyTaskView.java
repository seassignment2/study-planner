/******************************************************************************
 * File:        studyTaskView.java
 * Author(s):   Lisa Zhang
 * Edited by:   Ceri Rice
 * Date:        15/05/2017
******************************************************************************/
package studyplanner;

public class studyTaskView { public void printstudyTaskDetails(String Header, String Title, String ModuleCode, 
                                       int StartDay, int StartMonth, int StartYear,int Duration,
                                       String Type, String Content )
    {
            System.out.println("Study Task: ");
            System.out.println("Coursework: " + Title);
            System.out.println("Header: " + Header);
            System.out.println ("Module Code: " + ModuleCode );
            System.out.println("Start Date: " + StartDay +"/" + StartMonth + "/" + StartYear);
            System.out.println("Study Task Duration: " + Duration );
            System.out.println("Study Task Type: " + Type);
            System.out.println("The Content of This Task:" + Content);
   
            }          
    
}
