/******************************************************************************
 * File:        ExamView.java
 * Author(s):   Forire Song
 * Edited by:   Ceri Rice
 * Date:        15/05/2017
******************************************************************************/
package studyplanner;

public class ExamView {
    public void printExamDetails(String moduleCode, String examName, 
            String examRoom, int examDay, int examMonth, int examYear, 
            String examTime, int examDuration, int examValue){
        String Dzero;
        if(examDay <10) Dzero = "0";
        else Dzero ="";
        String Mzero;
        if(examMonth <10) Mzero = "0";
        else Mzero ="";
        
        System.out.println("Exam Details: ");
        System.out.println("Module Code: " + moduleCode);
        System.out.println("Exam: " + examName);
        System.out.println("Exam Room: " + examRoom);
        System.out.println("Exam Date : " + Dzero+examDay + "/" + Mzero+examMonth + "/" +
                examYear);
        System.out.println("Exam Time: " + examTime + "   Duration: " + 
                examDuration + "min");
        System.out.println("Exam Values: " + examValue + "%");
    }
}
