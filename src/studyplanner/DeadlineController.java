/******************************************************************************
 * File:        Deadline.java
 * Author(s):   Ceri Rice
 * Date:        16/05/2017
******************************************************************************/
package studyplanner;

public class DeadlineController {
    private Deadline model;
    private DeadlineView view;
    
    public DeadlineController(Deadline model, DeadlineView view){
        this.model = model;
        this.view = view;
    }
    
    public String getDeadlineKey(){
        return model.getKey();
    }
    public int getDeadlineDay(){
        return model.getDay();
    }
    public int getDeadlineMonth(){
        return model.getMonth();
    }
    public int getDeadlineYear(){
        return model.getYear();
    }
    public String getDeadlineTime(){
        return model.getHour();
    }
    public int getDeadlinePlace(){
        return model.getPlace();
    }
    
    public void setDeadlineKey(String deadlineKey){
        model.setKey(deadlineKey);
    }
    public void setDeadlineDate(int day, int month, int year){
        model.setDay(day);
        model.setMonth(month);
        model.setYear(year);
    }
    public void setDeadlineHour(String hour){
        model.setHour(hour);
    }
    public void setDeadlinePlace(int place){
        model.setPlace(place);
    }
    
    public void updateView(){
        view.printDeadlineDetails(model.getDay(), 
                                  model.getMonth(), model.getYear(),
                                  model.getHour());
    } 
}
