/******************************************************************************
 * File:        Module.java
 * Author(s):   Jack Baker
 * Date:        15/05/2017
******************************************************************************/
package studyplanner;

public class Module {
    private String moduleCode;
    private String title;
    private String lecturer;
    private String organiser;
    
    public Module(String moduleCode, String title, String lecturer, String organiser){
        this.moduleCode = moduleCode;
        this.title = title;
        this.lecturer = lecturer;
        this.organiser = organiser;
    }
    
    public String getModuleCode(){
        return moduleCode;
    }

    public String getTitle(){
        return title;
    }
    
    public String getLecturer(){
        return lecturer;
    }
    
    public String getOrganiser(){
        return organiser;
    }
    
    
    public void setModuleCode(String moduleCode){
        this.moduleCode = moduleCode;
    }
    
    public void setTitle(String title){
        this.title = title;
    }
    
    public void setLecturer(String lecturer){
        this.lecturer = lecturer;
    }
    
    public void setOrganiser(String organiser){
        this.organiser = organiser;
    }     
}