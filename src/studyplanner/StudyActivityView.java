/******************************************************************************
 * File:        StudyActivityView.java
 * Author(s):   Forire Song
 * Edited by:   Ceri Rice
 * Date:        15/05/2017
******************************************************************************/
package studyplanner;

/**
 *
 * @author Zero
 */
public class StudyActivityView {
    public void printStudyActivityDetails(String moduleCode, String saDescription,
            int day, int month, int year, int duration, String taskTitle, String milestoneTitle){
        System.out.println("Activity: " + saDescription);
        System.out.println("Date: " + day + "/" + month + "/" + year);
        System.out.println("Duration: " + duration + " min");
    }
    
}
