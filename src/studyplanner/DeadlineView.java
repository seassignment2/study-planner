/******************************************************************************
 * File:        Deadline.java
 * Author(s):   Ceri Rice
 * Date:        16/05/2017
******************************************************************************/
package studyplanner;

public class DeadlineView {
    public void printDeadlineDetails(int day, int month,
                                     int year, String hour){
        String Dzero;
        if(day <10) Dzero = "0";
        else Dzero ="";
        String Mzero;
        if(month <10) Mzero = "0";
        else Mzero ="";
        
        System.out.println("Deadline Details: ");
        System.out.println("Date of submission: " +Dzero+day+"/"+Mzero+month+"/"+year+".");
        System.out.println("Time of submission: " + hour);
    }
}
