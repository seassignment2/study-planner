/******************************************************************************
 * File:        ModuleController.java
 * Author(s):   Jack Baker
 * Edited by:   Ceri Rice
 * Date:        15/05/2017
******************************************************************************/
package studyplanner;

public class ModuleController {
    private Module model;
    private ModuleView view;
    
    public ModuleController(Module model, ModuleView view){
        this.model = model;
        this.view = view;
    }
    
    
    public String getModuleCode(){
        return model.getModuleCode();
    }
    
    public String getTitle(){
        return model.getTitle();
    }
    
    public String getLecturer(){
        return model.getLecturer();
    }
    
    public String getOrganiser(){
        return model.getOrganiser();
    }
    
    
    public void setModuleCode(String moduleCode){
        model.setModuleCode(moduleCode);
    }
    
    public void setTitle(String title){
        model.setTitle(title);
    }
    
    public void setLecturer(String lecturer){
        model.setLecturer(lecturer);
    }
    
    public void setOrganiser(String organiser){
        model.setOrganiser(organiser);
    }
    
    
    public void updateView(){
        view.printModuleDetails(model.getModuleCode(), model.getTitle(),
                                model.getLecturer(), model.getOrganiser());
    }   
}