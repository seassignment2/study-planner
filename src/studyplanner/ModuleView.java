/******************************************************************************
 * File:        ModuleView.java
 * Author(s):   Jack Baker
 * Edited by:   Ceri Rice
 * Date:        15/05/2017
******************************************************************************/
package studyplanner;

public class ModuleView{
    public void printModuleDetails(String moduleCode, String title,
                                        String lecturer, String organiser){
        
        System.out.println("Module Details: ");
        System.out.println("Module Code and title: " + moduleCode + ", " + title);
        System.out.println("Module Lecturer(s): " + lecturer);
        System.out.println("Module Organiser: " + organiser);
        
    }
    
}
