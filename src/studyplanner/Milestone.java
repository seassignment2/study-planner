/******************************************************************************
 * File:        Milestone.java
 * Author(s):   Forire Song
 * Edited by:   Ceri Rice
 * Date:        15/05/2017
******************************************************************************/
package studyplanner;
import java.util.*;

public class Milestone {
    private String moduleCode;
    private String milestoneType;
    private String milestoneTitle;
    private int day, month, year;
    private String milestoneGoal;
    private String tITLE;
    private int ID;
    
    public Milestone(String code, String type, String title, int day, int month,
            int year, String goal, String tITLE){
        this.moduleCode = code;
        this.milestoneType = type;
        this.milestoneTitle = title;
        this.day = day;
        this.month = month;
        this.year = year;
        this.milestoneGoal = goal; 
        this.tITLE = tITLE;
    }
    
    public String getModuleCode(){
        return moduleCode;
    }
    public String getType(){
        return milestoneType;
    }
    public String getTitle(){
        return milestoneTitle;
    }
    public int getDay(){
        return day;
    }
    public int getMonth(){
        return month;
    }
    public int getYear(){
        return year;
    }
    public String getGoal(){
        return milestoneGoal;
    }
    public String gettITLE(){
        return tITLE;
    }
    public int getID(){
        return ID;
    }
    
    public void setModuleCode(String moduleCode){
        this.moduleCode = moduleCode;
    }
    public void setType(String milestoneType){
        this.milestoneType = milestoneType;
    }
    public void setTitle(String milestoneTitle){
        this.milestoneTitle = milestoneTitle;
    }
    public void setGoal(String milestoneGoal){
        this.milestoneGoal = milestoneGoal;
    }
    public void setDay(int day){
        this.day = day;
    }
    public void setMonth(int month){
        this.month = month;
    }
    public void setYear(int year){
        this.year = year;
    }
    public void settITLE(String tITLE){
        this.tITLE = tITLE;
    }
    public void setPosition(SemesterProfileController SPC){
        boolean hasCode = false;
        boolean hasTitle = false;
        if("Exam".equals(milestoneType)){
            ArrayList<ExamController> ECArrayList = new ArrayList<ExamController>();
            for(int i = 0; i < SPC.getSemesterProfileExams().size();i++){
                ECArrayList.add((ExamController) SPC.getSemesterProfileExams().get(i));
            }
            for(int a = 0; a < ECArrayList.size();a++){
                if(this.moduleCode.equals(ECArrayList.get(a).getModuleCODE())){
                    hasCode = true;
                    if(this.tITLE.equals(ECArrayList.get(a).getExamNAME())){
                        ID = a;
                        hasTitle = true;
                    }
                    else
                        hasTitle = false;
                }
                else
                    hasCode = false;
            }   
        }
        else{
            ArrayList<CourseworkController> CCArrayList = new ArrayList<CourseworkController>();
            for(int i = 0; i < SPC.getSemesterProfileCourseworks().size();i++){
                CCArrayList.add((CourseworkController) SPC.getSemesterProfileCourseworks().get(i));
            }
            for(int a=0; a<SPC.getSemesterProfileCourseworks().size();a++){
                if(this.moduleCode.equals(CCArrayList.get(a).getModuleCode())){
                    hasCode = true;
                    if(this.tITLE.equals(CCArrayList.get(a).getCourseworkTitle())){
                        ID = a;
                        hasTitle = true;
                    }
                    else
                        hasTitle = false;
                }
                else
                    hasCode = false;
            }
        }
        if(hasCode == false)
            System.out.println("Module code does not match loaded data.");
        else if(hasTitle == false)
            System.out.println("Title does not match loaded data.");
    }
}


