/******************************************************************************
 * File:        MilestoneController.java
 * Author(s):   Forire Song
 * Edited by:   Ceri Rice
 * Date:        15/05/2017
******************************************************************************/
package studyplanner;

public class MilestoneController {
    
    private Milestone model;
    private MilestoneView view;
    
    public MilestoneController(Milestone model, MilestoneView view){
        this.model = model;
        this.view = view;
    }
        
    public void setModuleCODE(String moduleCode){
        model.setModuleCode(moduleCode);
    }
    
    public void setMilestoneTypeTilteGoal(String milestoneType, 
            String milestoneTitle, String milestoneGoal){
        model.setType(milestoneType);
        model.setTitle(milestoneTitle);
        model.setGoal(milestoneGoal);
    }
    
    public void setDate(int day, int month, int year){
        model.setDay(day);
        model.setMonth(month);
        model.setYear(year);
        
    }
    
    ///////////
    public void setMilesontetITLE(String tITLE){
        model.settITLE(tITLE);
    }
    public void setMilestoneID(SemesterProfileController SPC){
        model.setPosition(SPC);
    }
    //////////
    
    public String getModuleCODE(){
        return model.getModuleCode();
    }
    
    public String getMilestoneType(){
        return model.getType();
    }

    public String getMilestoneTitle(){
        return model.getTitle();
    }
  
    public int getMilestoneDay(){
        return model.getDay();
    }
    
    public int getMilestoneMonth(){
        return model.getMonth();
    }
    
    public int getMilestoneYear(){
        return model.getYear();
    }
    
    public String getMilestoneGoal(){
        return model.getGoal();
    }
    
    //////////
    public String getMilestontetITLE(){
        return model.gettITLE();
    }
    public int getMilestoneID(){
        return model.getID();
    }
    
    /////////
    
    public void updateView(){
        view.printMilestoneDetails(model.getModuleCode(), model.getType(), 
                model.getTitle(), model.getDay(), model.getMonth(),
                model.getYear(), model.getGoal());
    }
         
}