/******************************************************************************
 * File:        SemesterProfileController.java
 * Author(s):   Ceri Rice/Jack Baker
 * Date:        16/05/2017
******************************************************************************/

package studyplanner;

import java.util.ArrayList;

public class SemesterProfileController {
    private SemesterProfile model;
    private SemesterProfileView view;
    
    public SemesterProfileController(SemesterProfile model, 
                                     SemesterProfileView view){
        this.model = model;
        this.view = view;
    }
    
    public ArrayList getSemesterProfileModules(){
        return this.model.getModules();
    }
    public ArrayList getSemesterProfileCourseworks(){
        return this.model.getCourseworks();
    }
    public ArrayList getSemesterProfileDeadlines(){
        return this.model.getDeadlines();
    }
    public ArrayList getSemesterProfileExams(){
        return this.model.getExams();
    }
    public ArrayList getMilestones(){
        return this.model.getMilestones();
    }
    public ArrayList getTasks (){
        return this.model.getTasks();
    }
    public ArrayList getActivitys(){
        return this.model.getActivitys();
    }
    
    public void addSemesterProfileModules(ModuleController m){
        this.model.addModules(m);
    }
    public void addSemesterProfileCourseworks(CourseworkController c){
        this.model.addCourseworks(c);
    }
    public void addSemesterProfileDeadlines(DeadlineController d){
        this.model.addDeadlines(d);
    }
    public void addSemesterProfileExams(ExamController e){
        this.model.addExams(e);
    }
    public void addMilestones(MilestoneController m){
        this.model.addMilestones(m);
    }
    public void addTasks(studyTaskController t){
        this.model.addTasks(t);
    }
    public void addActivitys(StudyActivityController a){
        this.model.addActivitiys(a);
    }
    
    public void removeSemesterModule(ModuleController m){
        this.model.removeModule(m);
    }
    public void removeSemesterAll(){
        this.model.removeAll();
    }

    public void readFile(String location){
        this.model.readFile(location);
    }
    public void writeFile(){
        this.model.writeFile();
    }
    public void readWriteFile(){
        this.model.ReadWriteFile();
    }
    
    public void updateView(){
        view.printSemesterProfileDetails(this.model.getModules(), 
                this.model.getCourseworks(), this.model.getDeadlines(), 
                this.model.getExams());
    }
}
