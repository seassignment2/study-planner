/******************************************************************************
 * File:        Deadline.java
 * Author(s):   Ceri Rice
 * Date:        16/05/2017
******************************************************************************/
package studyplanner;

public class Deadline {
    private String moduleCode;
    private int day, month, year;
    private String hour;
    private int place;
    
    public Deadline(String code, int day, int month, int year, String hour){
        this.moduleCode = code;
        this.day = day;
        this.month = month;
        this.year = year;
        this.hour = hour;
    }
    
    public String getKey(){
        return moduleCode;
    }
    public int getDay(){
        return day;
    }
    public int getMonth(){
        return month;
    }
    public int getYear(){
        return year;
    }
    public String getHour(){
        return hour;
    }
    public int getPlace(){
        return place;
    }
    
    public void setKey(String code){
        this.moduleCode = code;
    }
    public void setDay(int day){
        this.day = day;
    }
    public void setMonth(int month){
        this.month = month;
    }
    public void setYear(int year){
        this.year = year;
    }
    public void setHour(String hour){
        this.hour = hour;
    }
    public void setPlace(int place){
        this.place = place;
    }
}
