/******************************************************************************
 * File:        MilestoneView.java
 * Author(s):   Forire Song
 * Edited by:   Ceri Rice
 * Date:        15/05/2017
******************************************************************************/
package studyplanner;

public class MilestoneView {
    public void printMilestoneDetails(String moduleCode, String milestoneType,
            String milestoneTitle, int day, int month, int year, 
            String milestoneGoal){
        System.out.println("Milestone details: ");
        System.out.println("Module Code: " + moduleCode);
        System.out.println("Milestone: " + milestoneType + "   -   " + 
                milestoneTitle);
        System.out.println("Completion Date: " + day + "/" + month + "/" + year);
        System.out.println("Milestone Goal: " + milestoneGoal);
        
     
    }
    
}
