/******************************************************************************
 * File:        studyTaskModel.java
 * Author(s):   Lisa Zhang
 * Edited by:   Ceri Rice
 * Date:        15/05/2017
******************************************************************************/
package studyplanner;
import java.util.*;

public class studyTaskModel {
    private String Title;
    private String ModuleCode;
    private int StartDay, StartMonth, StartYear;
    private int Duration;
    private String Type ;
    private String Content;
    private String Header;
    private int id;
    
   
    public studyTaskModel (String Title, String Code, int StartDay, int StartMonth, 
                            int StartYear, int Duration, 
                            String Type, String Content, String Header)
    {   
        this.Title = Title;
        this.ModuleCode = Code;
        this.StartDay = StartDay;
        this.StartMonth = StartMonth;
        this.StartYear = StartYear;
        this.Duration = Duration;
        this.Type = Type;
        this.Content = Content;
        this.Header = Header;
    }
            
    public String getTitle(){
        return Title;
    }
    
    public String getModuleCode(){
        return ModuleCode;
    }
    
    public int getStartDay(){
        return StartDay;
    }
    
    public int getStartMonth(){
        return StartMonth;
    }

    public int getStartYear(){
        return StartYear;
    }
    
    public int getDuration(){
        return Duration;
    }
    
    public String getType(){
        return Type;
    }
    
    public String getContent(){
        return Content;
    }
    public String getHeader(){
        return Header;
    }
    public int getid(){
        return id;
    }
    public void setTitle(String Title){
        this.Title = Title ;    
    }
    
    public void setModuleCode(String ModuleCode){
        this.ModuleCode = ModuleCode;
    }
    
    public void setStartDay(int StartDay){
        this.StartDay = StartDay;
    }
    
    public void setStartMonth(int StartMonth){
        this.StartMonth = StartMonth;
    }
    
    public void setStartYear(int StartYear){
        this.StartYear = StartYear;
    }
    
    public void setDuration(int Duration){
        this.Duration = Duration;
    
    }
    
    public void setType(String Type){
        this.Type = Type;
    }
    
    
    public void setContent(String Content){
        this.Content = Content;
    }
    public void settitle(String title){
        this.Header = title;
    }
     public void setPosition(SemesterProfileController SPC){
        boolean hasCode = false;
        boolean hasTitle = false;
        if("Exam".equals(Type)){
            ArrayList<ExamController> ECArrayList = new ArrayList();
            for(int i = 0; i < SPC.getSemesterProfileExams().size();i++){
                ECArrayList.add((ExamController) SPC.getSemesterProfileExams().get(i));
            }
            for(int a = 0; a < ECArrayList.size();a++){
                if(this.ModuleCode.equals(ECArrayList.get(a).getModuleCODE())){
                    hasCode = true;
                    if(this.Header.equals(ECArrayList.get(a).getExamNAME())){
                        id = a;
                        hasTitle = true;
                    }
                    else
                        hasTitle = false;
                }
                else
                    hasCode = false;
            }   
        }
        else{
            ArrayList<CourseworkController> CCArrayList = new ArrayList();
            for(int i = 0; i < SPC.getSemesterProfileCourseworks().size();i++){
                CCArrayList.add((CourseworkController) SPC.getSemesterProfileCourseworks().get(i));
            }
            for(int a=0; a<SPC.getSemesterProfileCourseworks().size();a++){
                if(this.ModuleCode.equals(CCArrayList.get(a).getModuleCode())){
                    hasCode = true;
                    if(this.Header.equals(CCArrayList.get(a).getCourseworkTitle())){
                        id = a;
                        hasTitle = true;
                    }
                    else
                        hasTitle = false;
                }
                else
                    hasCode = false;
            }
        }
        if(hasCode == false)
            System.out.println("Module code does not match loaded data.");
        else if(hasTitle == false)
            System.out.println("Title does not match loaded data.");
    }
}


    

