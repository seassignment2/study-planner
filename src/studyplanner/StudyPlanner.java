/******************************************************************************
 * File:        StudyPlanner.java
 * Author(s):   All
 * Date:        15/05/2017
******************************************************************************/

package studyplanner;

public class StudyPlanner {

    public static void main(String[] args) {
        String version = "1.0.0";
        System.out.println("Study Planner version: " + version);
                
        SemesterProfile SP = new SemesterProfile();
        SemesterProfileView SPView = new SemesterProfileView();
        SemesterProfileController SPC = new SemesterProfileController(SP, SPView);

        StudyPlannerGUI SPGUI = new StudyPlannerGUI(SPC);
        SPGUI.setVisible(true);
    }
}
