/******************************************************************************
 * File:        SemesterProfile.java
 * Author(s):   Ceri Rice/Jack Baker
 * Date:        16/05/2017
******************************************************************************/
package studyplanner;

import java.util.*;
import java.io.*;

public class SemesterProfile {
    private ArrayList<ModuleController> modules;
    private ArrayList<CourseworkController> courseworks;
    private ArrayList<DeadlineController> deadlines;
    private ArrayList<ExamController> exams;
    
    private ArrayList<MilestoneController> milestones;
    private ArrayList<studyTaskController> tasks;
    private ArrayList<StudyActivityController> activitys;
    
    public SemesterProfile(){
        this.modules = new ArrayList<ModuleController>();
        this.courseworks = new ArrayList<CourseworkController>();
        this.deadlines = new ArrayList<DeadlineController>();
        this.exams = new ArrayList<ExamController>();
        
        this.milestones = new ArrayList<MilestoneController>();
        this.tasks = new ArrayList<studyTaskController>();
        this.activitys = new ArrayList<StudyActivityController>();
    }
    public SemesterProfile(ArrayList<ModuleController> module,
                           ArrayList<CourseworkController> coursework,
                           ArrayList<DeadlineController> deadline, 
                           ArrayList<ExamController> exam,
                           ArrayList<MilestoneController> milestones,
                           ArrayList<studyTaskController> tasks,
                           ArrayList<StudyActivityController> activitys){
        this.modules = module;
        this.courseworks = coursework;
        this.deadlines = deadline;
        this.exams = exam;
        
        this.milestones = milestones;
        this.tasks = tasks;
        this.activitys = activitys;
    }
    
    public ArrayList<ModuleController> getModules(){
        return this.modules;
    }
    public ArrayList<CourseworkController> getCourseworks(){
        return this.courseworks;
    }
    public ArrayList<DeadlineController> getDeadlines(){
        return this.deadlines;
    }
    public ArrayList<ExamController> getExams(){
        return this.exams;
    }
    public ArrayList<MilestoneController> getMilestones(){
        return this.milestones;
    }
    public ArrayList<studyTaskController> getTasks(){
        return this.tasks;
    }
    public ArrayList<StudyActivityController> getActivitys(){
        return this.activitys;
    }
    
    public void addModules(ModuleController m){
        this.modules.add(m);
    }
    public void addCourseworks(CourseworkController c){
        this.courseworks.add(c);
    }
    public void addDeadlines(DeadlineController d){
        this.deadlines.add(d);
    }
    public void addExams(ExamController e){
        this.exams.add(e);
    }
    public void addMilestones (MilestoneController m){
        this.milestones.add(m);
    }
    public void addTasks (studyTaskController t){
        this.tasks.add(t);
    }
    public void addActivitiys (StudyActivityController a){
        this.activitys.add(a);
    }
    
    public void removeModule(ModuleController m){
        if(this.modules.contains(m)){
            this.modules.remove(m);
            for(int a=0; a<this.courseworks.size(); a++){
                if(this.courseworks.get(a).getModuleCode() == m.getModuleCode()){
                    this.courseworks.remove(a);
                }
            }
            for(int b=0; b<this.exams.size(); b++){
                if(this.exams.get(b).getModuleCODE() == m.getModuleCode()){
                    this.exams.remove(b);
                }
            }
        }
    }
    public void removeAll(){
        this.modules.clear();
        this.courseworks.clear();
        this.deadlines.clear();
        this.exams.clear();
    }

    public void readFile(String location){        
        try(BufferedReader br = new BufferedReader(new FileReader(location))){
            String line;
            int lineCount = 0;
            while((line = br.readLine()) !=null){
                lineCount++;
                String[] parts = line.split(",");
                if(null == parts[0])System.out.println("Error in semester file.");
                else switch (parts[0]) {
                    case "module":
                        Module MM = new Module(parts[1],parts[2],parts[3],parts[4]);
                        ModuleView MV = new ModuleView();
                        ModuleController MC = new ModuleController(MM, MV);
                        
                        this.modules.add(MC);
                        
                        break;
                    case "coursework":
                        //make dates usable.
                        int Sday, Smonth, Syear;
                        Sday = Integer.parseInt(parts[3].substring(0, 2));
                        Smonth = Integer.parseInt(parts[3].substring(3, 5));
                        Syear = Integer.parseInt(parts[3].substring(6, 10));
                        int Rday, Rmonth, Ryear;
                        Rday = Integer.parseInt(parts[6].substring(0, 2));
                        Rmonth = Integer.parseInt(parts[6].substring(3, 5));
                        Ryear = Integer.parseInt(parts[6].substring(6, 10));
                        
                        int Dday, Dmonth, Dyear;
                        Dday = Integer.parseInt(parts[4].substring(0, 2));
                        Dmonth = Integer.parseInt(parts[4].substring(3, 5));
                        Dyear = Integer.parseInt(parts[4].substring(6, 10));
                                                
                        CourseworkModel CM = new CourseworkModel(parts[1],parts[2],Sday,Smonth,Syear,Rday,Rmonth,Ryear,Integer.parseInt(parts[7]),parts[8],parts[9]);
                        CourseworkView CV = new CourseworkView();
                        CourseworkController CC = new CourseworkController(CM, CV);
                        
                        Deadline DM = new Deadline(parts[1],Dday,Dmonth,Dyear,parts[5]);
                        DeadlineView DV = new DeadlineView();
                        DeadlineController DC = new DeadlineController(DM, DV);
                        CC.setCourseworkDeadline(DC);
                        
                        this.courseworks.add(CC);
                        this.deadlines.add(DC);
                        
                        break;
                    case "exam":
                        //make dates usable.
                        int Eday, Emonth, Eyear;
                        Eday = Integer.parseInt(parts[4].substring(0, 2));
                        Emonth = Integer.parseInt(parts[4].substring(3, 5));
                        Eyear = Integer.parseInt(parts[4].substring(6, 10));
                        
                        Exam EM = new Exam(parts[1],parts[2],parts[3],Eday,Emonth,Eyear,parts[5],Integer.parseInt(parts[6]),Integer.parseInt(parts[7]));
                        ExamView EV = new ExamView();
                        ExamController EC = new ExamController(EM, EV);
                        
                        this.exams.add(EC);
                        
                        break;
                    default:
                        System.out.println("Error in semester file, on line: "
                                           + lineCount);
                        break;
                }
            }
            for(int i=0; i<this.deadlines.size(); i++){
                this.deadlines.get(i).setDeadlinePlace(i);
            }
        }
        catch(IOException e){
            System.out.println(e.getMessage());
        }
    }
    
    public void writeFile(){
        try{
            PrintWriter writer = new PrintWriter("M-ST-SA-Save.txt", "UTF-8");
            String holder = null;
            for(int a=0; a<this.milestones.size(); a++){
                holder = "milestone,"+
                        this.milestones.get(a).getModuleCODE()+","+
                        this.milestones.get(a).getMilestoneType()+","+
                        this.milestones.get(a).getMilestoneTitle()+","+
                        this.milestones.get(a).getMilestoneDay()+","+
                        this.milestones.get(a).getMilestoneMonth()+","+
                        this.milestones.get(a).getMilestoneYear()+","+
                        this.milestones.get(a).getMilestoneGoal()+","+
                        this.milestones.get(a).getMilestontetITLE()+"\n";
                writer.append(holder);
            }
            for(int b=0; b<this.tasks.size(); b++){
                holder = "task,"+
                        this.tasks.get(b).getTITLE()+","+
                        this.tasks.get(b).getModuleCODE()+","+
                        this.tasks.get(b).getStartDAY()+","+
                        this.tasks.get(b).getStartMONTH()+","+
                        this.tasks.get(b).getStartYEAR()+","+
                        this.tasks.get(b).getDURATION()+","+
                        this.tasks.get(b).getTYPE()+","+
                        this.tasks.get(b).getCONTENT()+","+
                        this.tasks.get(b).getHeader()+"\n";
                writer.append(holder);
            }
            for(int c=0; c<this.activitys.size(); c++){
                holder = "activity,"+
                        this.activitys.get(c).getModuleCODE()+","+
                        this.activitys.get(c).getSaDescription()+","+
                        this.activitys.get(c).getSaDay()+","+
                        this.activitys.get(c).getSaMonth()+","+
                        this.activitys.get(c).getSaYear()+","+
                        this.activitys.get(c).getSaDuration()+","+
                        this.activitys.get(c).getTaskTITLE()+","+
                        this.activitys.get(c).getMilestoneTITLE()+"\n";
                writer.append(holder);
            }
            writer.close();
        }
        catch(IOException e){
            System.out.println(e.getMessage());
        }
    }
    
    public void ReadWriteFile(){
        try(BufferedReader br = new BufferedReader(new FileReader("M-ST-SA-Save.txt"))){
            String line;
            while((line = br.readLine()) != null){
                String[] parts = line.split(",");
                if(null == parts[0]) System.out.println("Error in data loading.");
                else switch (parts[0]){
                    case "milestone":
                        Milestone MM = new Milestone(parts[1],parts[2],parts[3],Integer.parseInt(parts[4]),Integer.parseInt(parts[5]),Integer.parseInt(parts[6]),parts[7],parts[8]);
                        MilestoneView MV = new MilestoneView();
                        MilestoneController MC = new MilestoneController(MM,MV);
                        
                        this.milestones.add(MC);
                        break;
                    case "task":
                        studyTaskModel STM = new studyTaskModel(parts[1],parts[2],Integer.parseInt(parts[3]),Integer.parseInt(parts[4]),Integer.parseInt(parts[5]),Integer.parseInt(parts[6]),parts[7],parts[8],parts[9]);
                        studyTaskView STV = new studyTaskView();
                        studyTaskController STC = new studyTaskController(STM,STV);
                        
                        this.tasks.add(STC);
                        break;
                    case "activity":
                        StudyActivity SAM = new StudyActivity(parts[1],parts[2],Integer.parseInt(parts[3]),Integer.parseInt(parts[4]),Integer.parseInt(parts[5]),Integer.parseInt(parts[6]),parts[7],parts[8]);
                        StudyActivityView SAV = new StudyActivityView();
                        StudyActivityController SAC = new StudyActivityController(SAM,SAV);
                        
                        this.activitys.add(SAC);
                        break;
                    default:
                        System.out.println("Error in data loading.");
                        break;
                }
            }
        }
        catch(IOException e){
            System.out.println(e.getMessage());
        }
    }
}