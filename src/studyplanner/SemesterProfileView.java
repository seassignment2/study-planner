/******************************************************************************
 * File:        SemesterProfileView.java
 * Author(s):   Ceri Rice/Jack Baker
 * Date:        15/05/2017
******************************************************************************/
package studyplanner;

import java.util.ArrayList;

public class SemesterProfileView {
    public void printSemesterProfileDetails(ArrayList<ModuleController> module,
                                    ArrayList<CourseworkController> coursework, 
                                    ArrayList<DeadlineController> deadline, 
                                    ArrayList<ExamController> exam){
        boolean hasC = false;
        boolean hasE = false;
        System.out.println("Semester Profile Output: ");
        for(int a=0; a<module.size(); a++){
            module.get(a).updateView();
            String Mcode = module.get(a).getModuleCode();
            for(int b=0; b<coursework.size(); b++){
                if(coursework.get(b).getModuleCode().contains(Mcode)){
                    System.out.println("");
                    coursework.get(b).updateView();
                    hasC = true;
                }  
            }
            for(int c=0; c<exam.size(); c++){
                if(exam.get(c).getModuleCODE().contains(Mcode)){
                    System.out.println("");
                    exam.get(c).updateExamView();
                    hasE = true;
                }
            }  
            if(hasC == false)
                System.out.println("\nModule has no coursework.");
            if(hasE == false)
                System.out.println("\nModule has no Exam(s).");
            System.out.println("End of Module information.\n");
        }
    }
    
    public void printMSTDetails (ArrayList<MilestoneController> milestones, 
                                 ArrayList<studyTaskController> tasks,
                                 ArrayList<StudyActivityController> activitys){
        
        for (int d=0; d<milestones.size(); d++){
            milestones.get(d).updateView();
        }
        System.out.println("");
        for (int e=0; e<tasks.size(); e++){
            tasks.get(e).updateView();
        }
        System.out.println("");
        for (int f=0; f<activitys.size(); f++){
            activitys.get(f).updateStudyActivityView();
        }
    }
}
