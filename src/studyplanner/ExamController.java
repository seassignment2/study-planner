/******************************************************************************
 * File:        ExamController.java
 * Author(s):   Forire Song
 * Edited by:   Ceri Rice
 * Date:        15/05/2017
******************************************************************************/
package studyplanner;

public class ExamController {
    
    private Exam model;
    private ExamView view;
    public ExamController(Exam model, ExamView view){
        this.model = model;
        this.view = view;
    }
    
    public void setModuleCODE(String moduleCode){
        model.setModuleCode(moduleCode);
    }
    
    public void setExamNameModuleRoom(String examName, String examRoom){
        model.setExamName(examName);
        model.setExamRoom(examRoom);
    }
    
    public void setExamTIME(int examDay, int examMonth, int examYear, 
            String examTime){
        model.setExamDay(examDay);
        model.setExamMonth(examMonth);
        model.setExamYear(examYear);
        model.setExamTime(examTime);
    }
     public void setDurationValue(int examDuration, int examValue){
         model.setExamDuration(examDuration);
         model.setExamValue(examValue);
    }
     
    public String getModuleCODE(){
        return model.getModuleCode();
    }
    
    public String getExamNAME(){
        return model.getExamName();
    }
    
    public int getExamDAY(){
        return model.getExamDay();
    }
    
    public int getExamMONTH(){
        return model.getExamMonth();
    }
    public int getExamYEAR(){
        return model.getExamYear();
    }
    public String getExamTIME(){
        return model.getExamTime();
    }
    public int getExamDURATION(){
        return model.getExamDuration();
    }
    public int getExamVALUR(){
        return model.getExamValue();
    }

    
    public void updateExamView(){
        view.printExamDetails(model.getModuleCode(),
                model.getExamName(), model.getExamRoom(), model.getExamDay(), 
                model.getExamMonth(), model.getExamYear(), model.getExamTime(), 
                model.getExamDuration(), model.getExamValue());
    }

}
