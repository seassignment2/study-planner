/******************************************************************************
 * File:        StudyActivity.java
 * Author(s):   Forire Song
 * Edited by:   Ceri Rice
 * Date:        15/05/2017
******************************************************************************/
package studyplanner;

import java.util.*;

public class StudyActivity {
    private String moduleCode;
    private String saDescription;
    private int day, month, year, duration;
    private String tasktitle;
    private String milestonetitle;
    private int taskID;
    private int milestoneID;

    
    public StudyActivity(String code, String description, int day, int month,
            int year, int duration, String tasktitle, String milestonetitle){
        this.moduleCode = code;
        this.saDescription = description;
        this.day = day;
        this.month = month;
        this.year = year;
        this.duration = duration;
        this.tasktitle = tasktitle;
        this.milestonetitle = milestonetitle;
    }
    
    public String getModuleCode(){
        return moduleCode;
    }
    public String getDescription(){
        return saDescription;
    }
    public int getDay(){
        return day;
    }
    public int getMonth(){
        return month;
    }
    public int getYear(){
        return year;
    }
    public int getDuration(){
        return duration;
    }
    public int getTaskID(){
        return taskID;
    }
    public String getTaskTitle(){
        return tasktitle;
    }
    public String getMilestoneTitle(){
        return milestonetitle;
    }
    public int getMilestoneID(){
        return milestoneID;
    }
    
    
    public void setModuleCode(String moduleCode){
        this.moduleCode = moduleCode;
    }
    public void setDescripton(String saDescription){
        this.saDescription = saDescription;
    }
    public void setDay(int day){
        this.day = day;
    }
    public void setMonth(int month){
        this.month = month;
    }
    public void setYear(int year){
        this.year = year;
    }
    public void setDuration(int duration){
        this.duration = duration;
    }
    public void setTaskTitle(String tasktitle){
        this.tasktitle = tasktitle;
    }
    public void setMilestoneTitle(String milestonetitle){
        this.milestonetitle = milestonetitle;
    }
    public void setTaskIDPosition(SemesterProfile SPC){
        boolean hasCode = false;
        boolean hasTitle = false;
        ArrayList<studyTaskController> STCArrayList = new ArrayList<studyTaskController>();
        for(int i =0; i<SPC.getTasks().size();i++){
            STCArrayList.add((studyTaskController) SPC.getTasks().get(i));
        }
        for(int a = 0; a <STCArrayList.size();a++){
            if(this.moduleCode.equals(STCArrayList.get(a).getModuleCODE())){
                hasCode = true;
                if(this.tasktitle.equals(STCArrayList.get(a).getTITLE())){
                        taskID = a;
                        hasTitle = true;
                    }
                    else
                        hasTitle = false;
                }
                else
                    hasCode = false;
            }    
            if(hasCode == false)
                System.out.println("Module code does not match loaded data.");
            else if(hasTitle == false)
                System.out.println("Title does not match loaded data.");
        } 
    public void setMilestoneIDPosition(SemesterProfile SPC){
        boolean hasCode = false;
        boolean hasTitle = false;
        ArrayList<MilestoneController> MCArrayList = new ArrayList<MilestoneController>();
        for(int i =0; i<SPC.getMilestones().size();i++){
            MCArrayList.add((MilestoneController) SPC.getMilestones().get(i));
        }
        for(int a = 0; a <MCArrayList.size();a++){
            if(this.moduleCode.equals(MCArrayList.get(a).getModuleCODE())){
                hasCode = true;
                if(this.milestonetitle.equals(MCArrayList.get(a).getMilestoneTitle())){
                        milestoneID = a;
                        hasTitle = true;
                    }
                    else
                        hasTitle = false;
                }
                else
                    hasCode = false;
            }    
            if(hasCode == false)
                System.out.println("Module code does not match loaded data.");
            else if(hasTitle == false)
                System.out.println("Title does not match loaded data.");
    } 
}
