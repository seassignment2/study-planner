/******************************************************************************
 * File:        CourseworkModel.java
 * Author(s):   Lisa Zhang
 * Edited by:   Ceri Rice
 * Date:        15/05/2017
******************************************************************************/
package studyplanner;

public class CourseworkModel {
    
    private String moduleCode;
    private String courseworkTitle;
    private int day, month, year;
    private int returnDay, returnMonth, returnYear;
    private String organizer ;
    private int value;
    private String courseworkType;
    private DeadlineController courseworkDeadline;
    
   
    public CourseworkModel (String code, String title, int day, int month,
                            int year, int returnDay, int returnMonth, 
                            int returnYear, int value, String organizer,
                            String type)
    {   
        this.courseworkTitle = title;
        this.moduleCode = code;
        this.day = day;
        this.month = month;
        this.year = year;
        this.returnDay = returnDay;
        this.returnMonth = returnMonth;
        this.returnYear = returnYear;
        this.organizer = organizer;
        this.value = value;
        this.courseworkType = type;
    }
            
    public String getCourseworkTitle(){
        return courseworkTitle;
    }
    
    public String getMoudleCode(){
        return moduleCode;
    }
    
    public int getDay(){
        return day;
    }
    
    public int getMonth(){
        return month;
    }

    public int getYear(){
        return year;
    }
    
    public int getreturnDay(){
        return returnDay;
    }
    
    public int getreturnMonth(){
        return returnMonth;
    }
    
    public int getreturnYear(){
        return returnYear;
    }
    
    public String getorganizer(){
        return organizer;
    }
     
    public int getvalue(){
        return value;
    }
    
    public String getCourseworkType(){
        return courseworkType;
    }
    
    public DeadlineController GetDeadline(){
        return courseworkDeadline;
    }
    
    public void setCourseworkTitle(String courseworkTitle){
        this.courseworkTitle = courseworkTitle ;    
    }
    
    public void setModuleCode(String moduleCode){
        this.moduleCode = moduleCode;
    }
    
    public void setDay(int day){
        this.day = day;
    }
    
    public void setMonth(int month){
        this.month = month;
    }
    
    public void setYear(int year){
        this.year = year;
    }
    
    public void setReturnDay(int returnDay){
        this.returnDay = returnDay;
    
    }
    
    public void setReturnMonth(int returnMonth){
        this.returnMonth = returnMonth;
    }
    
    public void setReturnYear(int returnYear){
        this.returnYear = returnYear;    
    }
    
    public void setOrganizer(String organizer){
        this.organizer = organizer;
    }
    
    public void setValue (int value){
        this.value = value;
    }
    
    public void setCourseworkType(String courseworkType){
        this.courseworkType = courseworkType;
    }
    
    public void setDeandline(DeadlineController deadline){
        this.courseworkDeadline = deadline;
    }
}
    

