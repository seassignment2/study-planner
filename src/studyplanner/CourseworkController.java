/******************************************************************************
 * File:        CourseworkController.java
 * Author(s):   Lisa Zhang
 * Edited by:   Ceri Rice
 * Date:        15/05/2017
******************************************************************************/
package studyplanner;

public class CourseworkController {
    private CourseworkModel model;
    private CourseworkView view;
    
    public CourseworkController(CourseworkModel model, CourseworkView view){
        this.model = model;
        this.view = view;
    }
    
    public void setCourseworkTITLE(String courseworkTitle){
        model.setCourseworkTitle(courseworkTitle);
    }

    public void setModuleCODE(String moduleCode){
        model.setModuleCode(moduleCode);
    }
    public void setSettime(int day, int month, int year){
        model.setDay(day);
        model.setMonth(month);
        model.setDay(day);
    }
    public void setReturntime(int returnday, int returnmonth, int returnyear){
        model.setReturnDay(returnday);
        model.setReturnMonth(returnmonth);
        model.setReturnYear(returnyear);
    }
    public void setOrganizer( String organizer){
        model.setOrganizer(organizer);
    }
    public void setValue( int value){
        model.setValue(value);
    }
    public void setCourseTYPE(String courseworkType){
        model.setCourseworkType(courseworkType);
    }
    public void setCourseworkDeadline(DeadlineController deadline){
        model.setDeandline(deadline);
    }
    
    public String getCourseworkTitle(){
        return model.getCourseworkTitle();
    }
    public String getModuleCode(){
        return model.getMoudleCode();
    }
    public void updateView(){
        view.printCourseworkDetails(model.getMoudleCode(), model.getCourseworkTitle(), 
                model.getDay(), model.getMonth(), model.getYear(),
                model.getreturnDay(), model.getreturnMonth(), model.getreturnYear(),
                model.getorganizer(), model.getvalue(), model.getCourseworkType(), 
                model.GetDeadline());
    }

    
    
}