/******************************************************************************
 * File:        studyTaskController.java
 * Author(s):   Lisa Zhang
 * Edited by:   Ceri Rice
 * Date:        15/05/2017
******************************************************************************/
package studyplanner;

public class studyTaskController {
    private studyTaskModel model;
    private studyTaskView view;
    
    public studyTaskController(studyTaskModel model, studyTaskView view){
        this.model = model;
        this.view = view;
    }
    
    public void setTitle(String Title){
        model.setTitle(Title);
    }

    public void setModuleCode(String ModuleCode){
        model.setModuleCode(ModuleCode);
    }
    public void setStartDate(int StartDay, int StartMonth, int StartYear){
        model.setStartDay(StartDay);
        model.setStartMonth(StartMonth);
        model.setStartDay(StartDay);
    }
    public void setDuration(int Duration){
        model.setDuration(Duration);
       
    }
    
    public void setType( String Type){
        model.setType(Type);
    }
    public void setContent( String Content){
        model.setContent(Content);
    }
    public void setStudyTasktitle (String title){
        model.settitle(title);
    }
    public void setStudyTaskid(SemesterProfileController SPC){
        model.setPosition(SPC);
    }
    public String getHeader(){
        return model.getHeader();
    }
    public String getModuleCODE(){
        return model.getModuleCode();
    }
    public String getTITLE(){
        return model.getTitle();
    }
    public int getStartDAY(){
        return model.getStartDay();
    }
    public int getStartMONTH(){
        return model.getStartMonth();
    }
    public int getStartYEAR(){
        return model.getStartYear();
    }
    public String getTYPE(){
        return model.getType();
    }
    public int getDURATION(){
        return model.getDuration();
    }
    public String getCONTENT(){
        return model.getContent();
    }
    public int getID(){
        return model.getid();
    }
        
    public void updateView(){
        view.printstudyTaskDetails(model.getHeader(), model.getTitle(), model.getModuleCode(), 
                model.getStartDay(), model.getStartMonth(), model.getStartYear(),
                model.getDuration(), model.getType(), model.getContent());
                                
        
    }
}

