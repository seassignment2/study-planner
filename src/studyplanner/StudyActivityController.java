/******************************************************************************
 * File:        StudyActivityController.java
 * Author(s):   Forire Song
 * Edited by:   Ceri Rice
 * Date:        15/05/2017
******************************************************************************/
package studyplanner;

public class StudyActivityController {
    private StudyActivity model;
    private StudyActivityView view;
    
    public StudyActivityController(StudyActivity model, StudyActivityView view){
        this.model = model;
        this.view = view;
    }
    
    public void setModuleCODE(String moduleCode){
        model.setModuleCode(moduleCode);
    }
    
    public void setSaDescription(String saDescription){
        model.setDescripton(saDescription);
    }
    
    public void setDMYDurationTidMid(int day, int month, int year, int duration){
        model.setDay(day);
        model.setMonth(month);
        model.setYear(year);
        model.setDuration(duration);
    }
    public void setMilestoneTITLE(String milestonetitle){
        model.setMilestoneTitle(milestonetitle);
    }
    public void setTaskTITLE(String tasktitle){
        model.setTaskTitle(tasktitle);
    }
    public void setTASKID(SemesterProfile SPC){
        model.setTaskIDPosition(SPC);
    }
    public void setMILESTONEID(SemesterProfile SPC){
        model.setMilestoneIDPosition(SPC);
    }
    
    public String getModuleCODE(){
        return model.getModuleCode();
    }
    public String getSaDescription(){
        return model.getDescription();
    }
    public int getSaDay(){
        return model.getDay();
    }
    public int getSaMonth(){
        return model.getMonth();
    }
    public int getSaYear(){
        return model.getYear();
    }
    public int getSaDuration(){
        return model.getDuration();
    }
    public int getSaTaskID(){
        return model.getTaskID();
    }
    public int getSaMilestoneID(){
        return model.getMilestoneID();
    }
    public String getMilestoneTITLE(){
        return model.getMilestoneTitle();
    }
    public String getTaskTITLE(){
        return model.getTaskTitle();
    }
    public void updateStudyActivityView(){
        view.printStudyActivityDetails(model.getModuleCode(), model.getDescription(),
               model.getDay(), model.getMonth(), model.getYear(), model.getDuration(), 
               model.getTaskTitle(), model.getMilestoneTitle());
    }
    
    
    
    
    
}
