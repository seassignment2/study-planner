/******************************************************************************
 * File:        Exam.java
 * Author(s):   Forire Song
 * Edited by:   Ceri Rice
 * Date:        15/05/2017
******************************************************************************/
package studyplanner;

public class Exam {
    private String moduleCode;
    private String examName;
    private String examRoom;
    private int examDay, examMonth, examYear;
    private String examTime;
    private int examDuration;
    private int examValue;
    
    public Exam(String code, String name, String room, int day, 
            int month, int year, String time, int duration, int value){
        this.moduleCode = code;
        this.examName = name;
        this.examRoom = room;
        this.examDay = day;
        this.examMonth = month;
        this.examYear = year;
        this.examTime = time;
        this.examDuration = duration;
        this.examValue = value;
    }
    
    public String getModuleCode(){
        return moduleCode;
    }
    
    public String getExamName(){
        return examName;
    }
    
    public String getExamRoom(){
        return examRoom;
    }
    
    public int getExamDay(){
        return examDay;
    }
    
    public int getExamMonth(){
        return examMonth;
    }
    
    public int getExamYear(){
        return examYear;
    }
    
    public String getExamTime(){
        return examTime;
    }
    
    public int getExamDuration(){
        return examDuration;
    }
    
    public int getExamValue(){
        return examValue;
    }
    public void setModuleCode(String moduleCode){
        this.moduleCode = moduleCode;
    }
    
    public void setExamName(String examName){
        this.examName = examName;
    }
    
    public void setExamRoom(String examRoom){
        this.examRoom = examRoom;
    }
    
    public void setExamDay(int examDay){
        this.examDay = examDay;
    }
    
    public void setExamMonth(int examMonth){
        this.examMonth = examMonth;
    }
    
    public void setExamYear(int examYear){
        this.examYear = examYear;
    }
    
    public void setExamTime(String examTime){
        this.examTime = examTime;
    }
    
    public void setExamDuration(int examDuration){
        this.examDuration = examDuration;
    }
    
    public void setExamValue(int examValue){
        this.examValue = examValue;
    }
}
