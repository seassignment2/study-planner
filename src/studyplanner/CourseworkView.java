/******************************************************************************
 * File:        CourseworkView.java
 * Author(s):   Lisa Zhang
 * Edited by:   Ceri Rice
 * Date:        15/05/2017
******************************************************************************/
package studyplanner;

public class CourseworkView {
    public void printCourseworkDetails(String moduleCode, String moduleName, int day, int month, 
                                       int year, int returnDay, int returnMonth, 
                                       int returnYear, String organizer,
                                       int value, String courseworkType,
                                       DeadlineController deadline)
    {
        String Dzero;
        if(day <10) Dzero = "0";
        else Dzero ="";
        String Mzero;
        if(month <10) Mzero = "0";
        else Mzero ="";
        String RDzero;
        if(returnDay <10) RDzero = "0";
        else RDzero ="";
        String RMzero;
        if(returnMonth <10) RMzero = "0";
        else RMzero ="";
        
        System.out.println("Coursework Details: ");
        System.out.println("Module Code: " +moduleCode);
        System.out.println("Coursework: " + moduleName);
        System.out.println("Coursework Set Date: " + Dzero+day +"/" + Mzero+month + "/" + year);
        System.out.println("Coursework Return Date: " + RDzero+returnDay +"/" + RMzero+returnMonth + "/" + returnYear );
        System.out.println("Module Organizer: " + organizer);
        System.out.println("Coursework Value:" + value + "%");
        System.out.println("Coursework Type: " + courseworkType);
        deadline.updateView();
    }          
}
